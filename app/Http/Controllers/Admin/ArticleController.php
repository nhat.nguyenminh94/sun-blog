<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $category;
    public function __construct()
    {
        $this->category = Category::select()->get();
    }
    public function index()
    {

        $article = Article::with('category')->select()->paginate(2);
        return view('AdminPage.article.list', compact('article'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = $this->category;
        return view('AdminPage.article.addOrupdate', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        // dd($request->all());
        $this->handleStoreOrUpdate($request);
        return redirect()->route('admin.article.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article  = Article::findOrFail($id);
        $category = $this->category;
        return view('AdminPage.article.addOrupdate', compact('article', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->handleStoreOrUpdate($request, $id);
        return redirect()->route('admin.article.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();
        return redirect()->route('admin.article.list');
    }

    public function handleUploadImage($request)
    {
        if ($request->hasFile('input_img')) {
            $image           = $request->file('input_img');
            $name            = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $image->move($destinationPath, $name);
            return $name;
        }
        return false;
    }

    public function handleStoreOrUpdate1($request, $id = null)
    {
        $id != null ? $article = Article::findOrFail($id) : $article = new Article();
        $dataInput   = array_merge($request->all(), [
            'img'         => $this->handleUploadImage($request),
            'slug'        => str_slug($request->title),
            'category_id' => $request->select_category,
            'user_id'     => 1,
            'view'        => !$id ? $view = 1 : $view = $article->view,
        ]);
        $article->save();
        return redirect()->route('admin.article.list');
    }

    public function handleStoreOrUpdate($request, $id = null)
    {
        $dataInput = $request->all();
        $dataInput['img'] =  $this->handleUploadImage($request);
        $dataInput['slug'] = str_slug($dataInput['title']);
        $dataInput['category_id'] = $dataInput['select_category'];
        $dataInput['user_id'] = 1;
        $dataInput['view'] = 1;
        if($id){
            $model = Article::findOrFail($id);
            unset($dataInput['view']);
            $model->update($dataInput);
        }
        else{
            Article::create($dataInput);
        }
        return redirect()->route('admin.article.list');
    }
}
