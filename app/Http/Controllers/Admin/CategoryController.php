<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Validator;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::with('article')->select(['id','name','slug','status','created_at'])->paginate(2);  
        return view('AdminPage.category.list',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('AdminPage.category.addOrupdate');
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate(
        [   
            'name' => 'required',
        ],[
           'name.required' => 'Name Category ko được để trống',
       ]); 
      $category = new Category;
      $category->name = $request->name;
      if($request->status == "2")
      {
        $category->status = 2;
    }
    else
    {
        $category->status = 1;
    }
    $category->slug = str_slug($request->name); 
    $category->save();
    return redirect()->route('admin.category.list');
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(isset($id)){
            $name = Category::find($id);
        }
        return view('AdminPage.category.addOrupdate',compact('name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [   
                'name' => 'required',
            ],[
               'name.required' => 'Name Category ko được để trống',
           ]); 
        $category = Category::find($id);
        $category->name = $request->name;
        if($request->status == "2")
        {
            $category->status = 2;
        }
        else
        {
            $category->status = 1;
        }
        $category->slug = str_slug($request->name); 
        $category->save();
        return redirect()->route('admin.category.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect()->route('admin.category.list');
    }
}
