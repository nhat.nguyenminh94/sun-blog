<?php

namespace App\Http\Controllers\FrontPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use Response;
use View;
use Illuminate\Support\Facades\DB;
class ArticleController extends Controller
{
	public function index(Request $request){
		    $article = Article::select()->paginate(2);
		      if ($request->wantsJson() || $request->ajax()) {        
               return Response::json(View::make('FrontPage.content_article_load_more_ajax', array('article' => $article))->render());
                //ajax.contentUserLoadMore trỏ tới blade của bạn muốn truyền biến user vào
            }        

		 return view('FrontPage.index',compact('article'));
	}
    
    public function detail($slug,$id){
    	$article = Article::find($id);
    	if($article == null){
		 	return view('FrontPage.test');
    	}
    	return view('FrontPage.detail',compact('article'));
	}
	public function search(){
		if(isset($_GET['tag'])){
			$articleSearch = Article::where('tag','like','%'.$_GET['tag'].'%')->get();
		}else{
			$articleSearch = Article::where('title','like','%'.$_GET['search'].'%')->get();
		}
		isset($_GET['tag']) ? $abc = $_GET['tag'] : $abc = $_GET['search'];
		 return view('FrontPage.search.search',compact('articleSearch','abc'));
	}		
}
