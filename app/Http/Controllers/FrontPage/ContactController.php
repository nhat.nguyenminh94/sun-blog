<?php

namespace App\Http\Controllers\FrontPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use Validator;
class ContactController extends Controller
{
    public function index(){
    	return view('FrontPage.contact.createContact');
    }
    public function store(Request $request){
    	 $request->validate(
        [   
            'email' => 'required',
            'phone' => 'required',
            'content' => 'required',
        ],[
           'email.required' => 'email Contact ko được để trống',
           'phone.required' => 'phone  Contact ko được để trống',
       ]); 
      $contact = new Contact;
      $contact->email = $request->email;
      $contact->phone = $request->phone;
      $contact->content = $request->content;
    $contact->save();
    return redirect()->route('article.contact')->with('status','ok');
    }
}
