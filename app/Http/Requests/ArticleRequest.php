<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'required',
            'sub_content' => 'required',
            'content'     => 'required',
            'input_img'   => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
    public function messages()
    {
        return [
            'title.required'       => 'Tiêu đề không được để trống',
            'sub_content.required' => 'Tóm tắt không được để trống',
            'content.required'     => 'Nội Dung không được để trống',
            'input_img.required'     => 'Hình ảnh ko đúng định dạng',
        ];
    }
}
