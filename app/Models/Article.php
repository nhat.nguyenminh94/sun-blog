<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $fillable = [
        'id', 'title', 'slug','img','sub_content','content','status','view','tag','user_id','category_id'
    ];
       public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

      public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
