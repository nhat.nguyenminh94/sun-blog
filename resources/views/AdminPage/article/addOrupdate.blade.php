@extends('AdminPage.layouts.master')
@section('styles')
<script src="//cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
           <div class="card">
            <div class="card-header">
                <strong>Article</strong> Form
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif
          <div class="card-body card-block">
            <form action="@if(isset($article))
            {{route('admin.article.update',['id'=>$article->id])}}
            @else
            {{route('admin.article.created')}}
            @endif" method="post" class="" enctype="multipart/form-data">
            {{isset($article->title) ?   method_field('PUT') : '' }}
            @csrf
            <div class="form-group">
                <label for="name-cate" class=" form-control-label">Title</label>
                <input type="text" id="title" name="title" placeholder="Enter Title.." class="form-control" value="{{isset($article->title) ? $article->title : '' }}">
                <span class="help-block">Please enter your Title</span>
            </div>
            <div class="form-group">
                <label for="name-cate" class=" form-control-label">Sub-Content</label>
                <input type="text" id="sub_content" name="sub_content" placeholder="Enter Sub-Content.." class="form-control" value="{{isset($article->sub_content) ? $article->sub_content : '' }}">
                <span class="help-block">Please enter your Sub-content</span>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                <label for="name-cate" class=" form-control-label">Image</label>
                <input type="file" id="input_img" name="input_img" placeholder="Enter Image" class="form-control" value="{{isset($article->img) ? $article->img : '' }}">
                <span class="help-block">Please enter your Image</span>
                </div>
                <div class="col-md-6">
                    <img src="/uploads/{{isset($article->img) ? $article->img : 'upload.jpg' }}" alt="" style="width: 200px;height: 200px;">
                </div>
            </div>
             <div class="form-group">
                <label for="name-cate" class=" form-control-label">Category</label>
                 <select id="inputState" class="form-control" name="select_category">
                  @foreach($category as $value)
                  <option value="{{$value->id}}" {{isset($article) ? ($value->id == $article->category_id ? 'selected' : '') : '' }}>{{$value->name}}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name-cate" class=" form-control-label">Content</label>
                <textarea name="content" id="textarea-input" rows="3" placeholder="content" class="form-control ckeditor" value=""> {{isset($article->content) ? $article->content : '' }}</textarea>
                <script>
                CKEDITOR.replace( 'content' );
                </script>
            </div>            
             <div class="form-group">
                <label for="name-cate" class=" form-control-label">Tags</label>
                <input type="text" name="tag" value="{{isset($article->tag) ? $article->tag : '' }}" data-role="tagsinput">
            </div>
            <div class="form-group">
             <div class="row form-group">
                <div class="col col-md-3">
                    <label class=" form-control-label">Status</label>
                </div>
                <div class="col col-md-9">
                    <div class="form-check-inline form-check">
                        <label for="inline-radio1" class="form-check-label ">
                            <input type="radio" id="inline-radio1" name="status" value="1" class="form-check-input"  @if( isset($article) && $article->status == 1 )
                            checked
                            @elseif(!isset($article))
                            checked   
                            @endif>Active
                        </label>
                        <label for="inline-radio2" class="form-check-label ">
                            <input type="radio" id="inline-radio2" name="status" value="2" class="form-check-input" @if(isset($article) && $article->status == 2 )
                            checked
                            @endif >Not Active
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-dot-circle-o"></i> Submit
            </button>
            <button type="reset" class="btn btn-danger btn-sm">
                <i class="fa fa-ban"></i> Reset
            </button>
        </div>
    </form>
</div>

</div>
</div>
</div>
</div>

@endsection
@section('scripts')
<script src="/admin-css/js/tagsinput.js"></script>
@endsection