@extends('AdminPage.layouts.master')
@section('content')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="#">Dashboard</a>
	</li>
	<li class="breadcrumb-item active">Tables</li>
</ol>
<!-- Example DataTables Card-->
<div class="card mb-3">
	<div class="card-header">
		<i class="fa fa-table"></i> Data Table Example</div>
		<div class="row">
			<div class="col-md-6">
				<a class="btn btn-success text-center" href="{{route('admin.article.add')}}" style="width: 100%;margin-bottom: 10px;">
				Add </a>
			</div>         
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>ID</th>
							<th>title</th>		
							<th >sub_content</th>
							<th >status</th>		
							<th >view</th>		
							<th >category</th>		
							<th >user</th>		
							<th >date</th>
							<th colspan="2">Control</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>ID</th>
							<th>title</th>		
							<th >sub_content</th>
							<th >status</th>		
							<th >view</th>		
							<th >category</th>		
							<th >user</th>		
							<th >date</th>
							<th colspan="2">Control</th>
						</tr>
					</tfoot>
					<tbody>
						@foreach($article as $val)
						<tr>
							<td>{{$val->id}}</td>
							<td>{{$val->title}}</td>
							<td>{{$val->sub_content}}</td>
							<td>
								{{$val->status == "2" ? "Not Active" : "Active"}}
							</td>
							<td>{{$val->view}}</td>
							<td>{{$val->category->name}}</td>
							<td>{{$val->user_id}}</td>
							<td>{{date('d-m-Y', strtotime($val->created_at))}}</td>
							<td><a class="btn btn-primary" href="{{route('admin.article.edit',['id'=>$val->id])}}">Update</a></td>
							<td>
								<form action="{{route('admin.article.destroy',['id'=>$val->id])}}" method="POST">
									@method('DELETE')
									@csrf
									<button class="btn btn-danger">Delete</button>
								</form>  
							</td>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<div class="row justify-content-md-center" >          
			{!! $article->links() !!}
		</div>
	</div>
	@endsection