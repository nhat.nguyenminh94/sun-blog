@extends('AdminPage.layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
           <div class="card">
            <div class="card-header">
                <strong>Normal</strong> Form
            </div>


            @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif
          <div class="card-body card-block">
            <form action="@if(isset($name))
            {{route('admin.category.update',['id'=>$name->id])}}
            @else
            {{route('admin.category.created')}}
            @endif" method="post" class="">
            {{isset($name->name) ?   method_field('PUT') : '' }}
            @csrf
            <div class="form-group">
                <label for="name-cate" class=" form-control-label">Category Name</label>
                <input type="text" id="name" name="name" placeholder="Enter Name.." class="form-control" value="{{isset($name->name) ? $name->name : '' }}">
                <span class="help-block">Please enter your Category</span>
            </div>
            <div class="form-group">
             <div class="row form-group">
                <div class="col col-md-3">
                    <label class=" form-control-label">Status</label>
                </div>
                <div class="col col-md-9">
                    <div class="form-check-inline form-check">
                        <label for="inline-radio1" class="form-check-label ">
                            <input type="radio" id="inline-radio1" name="status" value="1" class="form-check-input"  @if( isset($name) && $name->status == 1 )
                            checked
                            @elseif(!isset($name))
                            checked   
                            @endif>Active
                        </label>
                        <label for="inline-radio2" class="form-check-label ">
                            <input type="radio" id="inline-radio2" name="status" value="2" class="form-check-input" @if(isset($name) && $name->status == 2 )
                            checked
                            @endif >Not Active
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-dot-circle-o"></i> Submit
            </button>
            <button type="reset" class="btn btn-danger btn-sm">
                <i class="fa fa-ban"></i> Reset
            </button>
        </div>
    </form>
</div>

</div>
</div>
</div>
</div>

@endsection