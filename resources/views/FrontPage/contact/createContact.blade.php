@extends('FrontPage.layouts.master')
@section('content')
  <div class="col-md-9  col-lg-9">
          <div class="blog-post  blog-post--fullwidth  blog-post--page">
            <div class="blog-post__image">
              <img src="/front-css/img/contact.jpg" alt="Contact">
            </div>
            <div class="blog-post-wrapper">
              <div class="blog-post__title">
                <h1>Contact</h1>
              </div>
              <div class="blog-post__content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <a href="#">Suspendisse lobortis</a> commodo ullamcorper. Duis pretium convallis odio non varius. Nulla quis lorem metus. Fusce velit magna, ultricies quis elit sit amet, lacinia porttitor magna. Nulla scelerisque, ex quis imperdiet finibus, elit massa dictum arcu, et fermentum orci felis in ipsum. Suspendisse rutrum nec ipsum id feugiat. Nunc eu lacinia turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam dapibus cursus justo.</p>
              </div>

               @if (session('status'))
                  <div class="alert alert-success">
                  <strong>Thành Công!</strong> Bạn đã gửi contact.
                  </div>
                @endif
              <div class="blog-post__contact">
              
                <form action="{{route('article.contact.created')}}" method="post">
                	@csrf
                  <p class="blog-post__contact-email">
                    <label for="contact-email">Email*</label>
                    <input id="contact-email" type="email" name="email" size="30" aria-required="true" required>
                  </p>
                   <p class="blog-post__contact-email">
                    <label for="contact-email">Phone*</label>
                    <input id="contact-email" type="number" name="phone" size="30" aria-required="true" required>
                  </p>
                  <p class="blog-post__contact-message">
                    <label for="contact-message">Message</label>
                    <textarea id="contact-message" name="content" cols="45" aria-required="true"></textarea>
                  </p>
                  <p class="blog-post__contact-submit">
                    <input id="contact-submit" type="submit" name="contact-submit" size="30" value="Send">
                  </p>
                </form>
              </div>
              <div class="blog-post__footer">
                <div class="blog-post__footer-social">
                  <span>Share:</span>
                  <div class="blog-post__footer-social-icons">
                    <a href="#">
                      <svg>
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
                      </svg>
                    </a>
                    <a href="#">
                      <svg>
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-twitter"></use>
                      </svg>
                    </a>
                    <a href="#">
                      <svg>
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-google"></use>
                      </svg>
                    </a>
                    <a href="#">
                      <svg>
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-pinterest"></use>
                      </svg>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection