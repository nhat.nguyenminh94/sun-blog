@extends('FrontPage.layouts.master')
@section('content')
        <div class="col-md-9 load_more">
          @foreach($article as $val)
          <div class="blog-post">
            <div class="blog-post-wrapper">
              <div class="blog-post__info  blog-post__info--top">
                <span><a href="#">{{$val->category->name}}</a></span>
              </div>
              <div class="blog-post__title">
                <h2><a href="single-post.html">{{$val->title}}</a></h2>
              </div>
              <div class="blog-post__info">
                <span>By <a href="#">Yuta</a></span>
                <span>{{date_format($val->created_at,"M-d/-y ")}}</span>
                <span><a href="#">1 Comments</a></span>
              </div>
            </div>
            <div class="blog-post__image">
              <a href="single-post.html"><img src="/front-css/img/img0.jpg" alt="Why Glass Had Been So Popular Till Now?"></a>
            </div>
            <div class="blog-post-wrapper">
              <div class="blog-post__content">
                <p>{{$val->sub_content}}</p>
              </div>
              <div class="blog-post__footer">
                <a class="blog-post__footer-link" href="{{route('article.detail',['slug'=>$val->slug,'id'=>$val->id])}}">Read more</a>
                <div class="blog-post__footer-social">
                  <span>Share:</span>
                  <div class="blog-post__footer-social-icons">
                    <a href="#">
                      <svg>
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
                      </svg>
                    </a>
                    <a href="#">
                      <svg>
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-twitter"></use>
                      </svg>
                    </a>
                    <a href="#">
                      <svg>
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-google"></use>
                      </svg>
                    </a>
                    <a href="#">
                      <svg>
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-pinterest"></use>
                      </svg>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          <nav class="blog-pagination">
            <ul class="blog-pagination__items">
<!--               <li class="blog-pagination__item  blog-pagination__item--active">
                <a>1</a>
              </li>
              <li class="blog-pagination__item">
                <a href="#">2</a>
              </li> -->
              <li class="blog-pagination__item">
                <a href="javascript:loadmore()">Old Post</a>
              </li>
            </ul>
          </nav>
        </div>
@endsection
@section('scripts')
<script>
  var page = 2;
  function loadmore() {
    $.ajax({
      type: "GET",
      url: "/?page="+page,
      cache: false,
      success: function(data){
        console.log(data);
         $(".blog-post").append(data);
         page = page + 1;
      }
    });
  }
</script>
@endsection