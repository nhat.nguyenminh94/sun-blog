    <header class="header  container">
      <div class="row  header__logo">
        <div class="col-md-3">
          <div class="social-icons">
            <a href="#">
              <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
              </svg>
            </a>
            <a href="#">
              <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-twitter"></use>
              </svg>
            </a>
            <a href="#">
              <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-instagram"></use>
              </svg>
            </a>
          </div>
        </div>
        <div class="col-md-6  logo">
          <h1><a class="logo__link" href="/">Yuta Nhật.</a></h1>
          <div class="logo__description">Personal Blog Template</div>
        </div>
        <div class="col-md-3  header__right">
          <div class="top-icons">
            <div class="top-icons__search">
              <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search"></use>
              </svg>
            </div>
          </div>
        </div>
      </div>
      <nav class="nav">
        <ul class="nav__list">
          <li class="nav__item  nav__item--active">
            <a href="/">Home</a>
            <ul class="nav__list-submenu">
              <li class="nav__item-submenu">
                <a href="index-fullwidth.html">Home Fullwidth</a>
              </li>
              <li class="nav__item-submenu">
                <a href="index-grid.html">Home Grid</a>
                <ul class="nav__list-submenu">
                  <li class="nav__item-submenu">
                    <a href="index-grid-fullwidth.html">Home Grid Fullwidth</a>
                  </li>
                </ul>
              </li>
              <li class="nav__item-submenu">
                <a href="index-list.html">Home List</a>
                <ul class="nav__list-submenu">
                  <li class="nav__item-submenu">
                    <a href="index-list-fullwidth.html">Home List Fullwidth</a>
                  </li>
                </ul>
              </li>
              <li class="nav__item-submenu">
                <a href="404.html">404 Page</a>
              </li>
              <li class="nav__item-submenu">
                <a href="single-post.html">Single Post</a>
                <ul class="nav__list-submenu">
                  <li class="nav__item-submenu">
                    <a href="single-post-fullwidth.html">Single Post Fullwidth</a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="nav__item">
            <?php $category = DB::table('categories')->select(['id','name'])->get();
             ?>
            <a href="category.html">Category</a>
            <ul class="nav__list-submenu">
              @foreach($category as $value )
              <li class="nav__item-submenu">
                <a href="{{$value->id}}">{{$value->name}}</a>
              </li>
              @endforeach
            </ul>
          </li>
          <li class="nav__item">
            <a href="category.html">Journey</a>
          </li>
          <li class="nav__item">
            <a href="category.html">Inspiration</a>
          </li>
          <li class="nav__item">
            <a href="about.html">About Me</a>
          </li>
          <li class="nav__item">
            <a href="{{route('article.contact')}}">Contact</a>
          </li>
        </ul>
      </nav>
      <div class="nav-toggle">
        <svg class="nav-toggle__icon">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-toggle"></use>
        </svg>
      </div>
    </header>