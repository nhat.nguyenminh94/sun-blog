<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Yuta. Home</title>
    <link rel="shortcut icon" href="/front-css/img/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">
    <link href="/front-css/css/bootstrap.min.css" rel="stylesheet">
    <link href="/front-css/css/jquery.bxslider.css" rel="stylesheet">
    <link href="/front-css/css/style.min.css" rel="stylesheet">
    @yield('styles')
  </head>
  <body>
   @include('FrontPage.layouts.search')
   @include('FrontPage.layouts.header')
   @include('FrontPage.layouts.slider')
  
    <main>
      <div class="container">
              @yield('content')
      @include('FrontPage.layouts.menu')
      </div>
    </main>
       @include('FrontPage.layouts.footer')
    <div class="search-popup">
      <svg class="search-popup__close">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-close"></use>
      </svg>
      <div class="search-popup__container  container">
        <form action="{{route('article.search')}}">
          <input type="text" size="32" name="search" placeholder="Search">
        </form>
      </div>
	</div>
    <div class="content-overlay"></div>
    <script src="/front-css/js/jquery-3.1.1.min.js"></script>
    <script src="/front-css/js/jquery.slicknav.min.js"></script>
    <script src="/front-css/js/jquery.bxslider.min.js"></script>
    <script src="/front-css/js/script.js"></script>
    @yield('scripts')
  </body>
</html>
