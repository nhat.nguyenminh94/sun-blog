  <section class="container  top-slider">
      <div class="col-md-12">
        <div class="bxslider">
          <div>
            <div class="featured-image" style="background-image:url(/front-css/img/img1.jpg);">
              <div class="featured-image__bg"></div>
              <div class="featured-image__content">
                <span class="featured-image__content-category">
                  <a href="#">Lifestyle</a>
                </span>
                <span class="featured-image__content-title">
                  <a href="single-post.html">Fall In Love With Cat</a>
                </span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lobortis commodo ullamcorper.</p>
                <span class="featured-image__content-link">
                  <a href="single-post.html">Read More</a>
                </span>
              </div>  
            </div>
          </div>
          <div>
            <div class="featured-image" style="background-image:url(/front-css/img/img2.jpg);">
              <div class="featured-image__bg"></div>
              <div class="featured-image__content">
                <span class="featured-image__content-category">
                  <a href="#">Journey</a>
                </span>
                <span class="featured-image__content-title">
                  <a href="single-post.html">All You Need To Know About Forest</a>
                </span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lobortis commodo ullamcorper.</p>
                <span class="featured-image__content-link">
                  <a href="single-post.html">Read More</a>
                </span>
              </div> 
            </div>
          </div>
          <div>
            <div class="featured-image" style="background-image:url(/front-css/img/img3.jpg);">
              <div class="featured-image__bg"></div>
              <div class="featured-image__content">
                <span class="featured-image__content-category">
                  <a href="#">Lifestyle</a>
                </span>
                <span class="featured-image__content-title">
                  <a href="single-post.html">Skills That You Can Learn From Book</a>
                </span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lobortis commodo ullamcorper.</p>
                <span class="featured-image__content-link">
                  <a href="single-post.html">Read More</a>
                </span>
              </div> 
            </div>
          </div>
        </div>
      </div>
    </section>