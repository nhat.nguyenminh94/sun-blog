@extends('FrontPage.layouts.master')
@section('content')
  <div class="col-md-9  container--list">
          <div class="category-title">
            <h1>{{$abc}}</h1>
          </div>
          @foreach($articleSearch as $val)
          <div class="blog-post  blog-post--list">
            <div class="row">
              <div class="col-sm-6  col-md-6  blog-post__image">
                <a href="single-post.html"><img src="/front-css/img/img0.jpg" alt="Why Glass Had Been So Popular Till Now?"></a>
              </div>
              <div class="col-sm-6  col-md-6  post__list-content">
                <div class="blog-post__info  blog-post__info--date">
                  <span>{{date_format($val->created_at,"M-d/-y ")}}</span>
                  <span><a href="#">{{$val->category->name}}</a></span>
                  <span><a href="#">1 Comments</a></span>
                </div>
                <div class="blog-post__title">
                  <h2><a href="single-post.html">{{$val->title}}</a></h2>
                </div>
                <div class="blog-post__content">
                  <p>{{$val->sub_content}}</p>
                </div>
                <div class="blog-post__footer">
                  <a class="blog-post__footer-link" href="{{route('article.detail',['slug'=>$val->slug,'id'=>$val->id])}}">Read more</a>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          <nav class="blog-pagination">
            <ul class="blog-pagination__items">
              <li class="blog-pagination__item  blog-pagination__item--active">
                <a>1</a>
              </li>
              <li class="blog-pagination__item">
                <a href="#">2</a>
              </li>
              <li class="blog-pagination__item">
                <a href="#">Next Page</a>
              </li>
            </ul>
          </nav>
        </div>
@endsection