<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['prefix' => '', 'namespace' => 'FrontPage'], function () {
       Route::get('/', 'ArticleController@index')->name('article.index');
       Route::get('/detail/{slug}_{id}', 'ArticleController@detail')->name('article.detail');
       Route::get('/search', 'ArticleController@search')->name('article.search');
       Route::get('/contact', 'ContactController@index')->name('article.contact');
       Route::post('/contact/created', 'ContactController@store')->name('article.contact.created');
});
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'CategoryController@index')->name('admin.category.list');
        Route::get('/add', 'CategoryController@create')->name('admin.category.add');
        Route::post('/created', 'CategoryController@store')->name('admin.category.created');
        Route::get('edit/{id}', 'CategoryController@edit')->name('admin.category.edit');
        Route::put('/{id}', 'CategoryController@update')->name('admin.category.update');
        Route::delete('/category/{id}', 'CategoryController@destroy')->name('admin.category.destroy');
    });
    Route::group(['prefix' => 'contact'], function () {
        Route::get('/', 'ContactController@index')->name('admin.contact.list');
        Route::delete('/contact/{id}', 'ContactController@destroy')->name('admin.contact.destroy');
    });
     Route::group(['prefix' => 'article'], function () {
        Route::get('/', 'ArticleController@index')->name('admin.article.list');
         Route::get('/add', 'ArticleController@create')->name('admin.article.add');
        Route::post('/created', 'ArticleController@store')->name('admin.article.created');
        Route::get('edit/{id}', 'ArticleController@edit')->name('admin.article.edit');
        Route::put('/{id}', 'ArticleController@update')->name('admin.article.update');
        Route::delete('/article/{id}', 'ArticleController@destroy')->name('admin.article.destroy');
    });
});

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
